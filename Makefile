CC = gcc
CFLAGS = -O3 -Wall -Wextra -Werror -Wno-unknown-pragmas -g -fopenmp

all: stream

stream: stream.c Makefile
	$(CC) $(CFLAGS) stream.c -o $@

clean:
	rm -f stream *.o
